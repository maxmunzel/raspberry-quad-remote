from __future__ import print_function
from pyMultiwii import MultiWii
from json import loads
from websocket_server import WebsocketServer
from sys import exit


serial_port = ["/dev/cu.usbserial-A104QE4D", "/dev/cu.SLAB_USBtoUART"][1]
q = MultiWii(serial_port)
print ("Opened Device! on port", serial_port)

def failsafe(*args):
    q.disarm()
    print ("Failsafe activated!")
    exit(2)

if __name__ == "__main__":

    def handler(client, server, msg):

        throttle, yaw, roll, pitch, arm = loads(msg)
        print (msg, q.arm_val)
        if arm == 2000:
            q.arm()
        elif arm == 1000:
            q.disarm()

            q.sendControlData(throttle, yaw, pitch, roll)

    server = WebsocketServer(9000)
    server.set_fn_message_received(
        #lambda client, server, msg: q.sendControlData(*loads(msg))
        handler
    )
    server.set_fn_client_left(failsafe)
    try:
        server.run_forever()
    finally:
        q.ser.close()
