#  raspberry-quad-remote   

Use those scripts to control your CleanFlight (or similar) quadcopter using a PS4 controller over wifi using python, web sockets and a healthy bit of insanity.

## Disclaimer

This is all very alpha and is neither able, nor trying to be a sane replacement for a standard RC-remote for normal use. It is much more designed to be a proof of concept and a way to get easy scripting and hacking on top of the CleanFlight stack.