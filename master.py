from __future__ import print_function
import pygame
import time
import os
import json
from websocket import create_connection


def clear():
    os.system("clear")


def draw_bar(name, val):
    # type: (str, int) -> None
    print(name + ":\t\t" + "#" * int((1 + val) * 40))


pygame.init()
pygame.joystick.init()
js = pygame.joystick.Joystick(0)
ws = create_connection(["ws://localhost:9000", "ws://192.168.0.178:9000"][1])
print("Found gamepad")
js.init()
print("Initialized " + js.get_name())
# throttle ->   axis 1 (INVERTED)
# yaw ->        axis 0
# roll ->       axis 2
# pitch ->      axis 3 (INVERTED)


def smooth_throttle(throttle, val):
    val **= 3
    val /= 7
    throttle += val
    if throttle < -1:
        throttle = -1
    if throttle > 1:
        throttle = 1
    return throttle


try:
    throttle = 0
    while True:
        pygame.event.pump()
        if js.get_button(10):
            print ("DISARMED")
            arm = -1
        elif js.get_button(11):
            print ("ARMING")
            arm = 1
        else:
            arm = 0

        yaw, roll, pitch = map(lambda x: js.get_axis(x), (0, 2, 3))
        throttle = smooth_throttle(throttle, js.get_axis(1))
        inputs = [-throttle, yaw, roll, -pitch, arm]

        payload = map(lambda x: 1000 + (x + 1) * 500, inputs)

        payload = map(int, payload)

        ws.send(json.dumps(payload))
        clear()
        draw_bar("thro", inputs[0])
        draw_bar("yaw", inputs[1])
        draw_bar("roll", inputs[2])
        draw_bar("pitch", inputs[3])
        time.sleep(1.0 / 30)
except KeyboardInterrupt:
    pass
finally:
    ws.close()
