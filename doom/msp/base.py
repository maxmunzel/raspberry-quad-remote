import serial
from time import sleep

class MSP:

    def __init__(self, port, baudrate):
        self.ser = serial.Serial(port=port, baudrate=baudrate)
        self.checksum = 0

    def close(self):
        self.ser.close()

    def send_rc (self, channel_vals):
        def write_num16(n):
            f =  lambda x: map(chr, divmod(x, 255))
            self.ser.write(f(n))
            for char in divmod(n, 255):
                self.checksum ^= char

        if len(channel_vals) != 8:
            raise ValueError("Expect exactly 8 vals - ROLL/PITCH/YAW/THROTTLE/AUX1/AUX2/AUX3AUX4 ")

        size = 32
        self.checksum ^= size
        self.ser.write(b"$M<")
        self.ser.write(chr(1))
        self.checksum ^= 1
        self.ser.write(bytes(201))
        for channel in channel_vals:
            write_num16(channel)

        self.ser.write(chr(self.checksum))


if __name__ == "__main__":
    msp = MSP("/dev/cu.usbserial-A104QE4D", 115200)
    for i in range(20):
        msp.send_rc([1000]*8)
        sleep(1)
    msp.close()



