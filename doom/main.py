import pygame
import time
pygame.init()
pygame.joystick.init()
js = pygame.joystick.Joystick(0)
print ("Found gamepad")
js.init()
print ("Initialized " + js.get_name())
while True:
    pygame.event.pump()
    print "#" * int ((1 + js.get_axis(0)) * 20)
    time.sleep(1.0/60)